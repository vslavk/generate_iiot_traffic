//header file with some c, cpp libraries and definitions
#include "hdr/libs.hpp"
//Random variable generators classes
#include "hdr/generate_random_values.hpp"
//Message sender classes for different transport and application layers
#include "hdr/message_senders.hpp"
//Some classes to read JSON configuration files/HTTP-messages
#include "hdr/read_configuration.hpp"
//Configuration server class which build by existent configuration C server
#include "hdr/configuration_server.hpp"
//Some additional functions
#include "hdr/internal_functions.hpp"

int main(int argc, char **argv) {
	//Set seed for random generator
	srand(time(NULL));

	size_t input_type = 0;
	std::string input_var = "NULL", port = "0";

	if(argc < 2)
	{
		fprintf(stderr, "Too few arguments!\n");
		exit(1);
	}
	//Command line arguments processing
	if(internal_functions::read_command_line_arguments(argc, argv, &input_type, &input_var, &port) != 0)
	{
		fprintf(stderr, "Error in arguments!\n");
		exit(1);
	}

	//if input_type == 1 - start handler for configuration files
	if(input_type == 1)
	{
		std::unique_ptr<read_configuration> distr_config = std::make_unique<read_configuration>(input_var);
		internal_functions::configure_message_sender(std::move(distr_config));
	}
	//if input_type == 2 - start handler for configuration server
	else if (input_type == 2)
	{
		configuration_server conf_ser(input_var.c_str(), port.c_str());
		conf_ser.serve_forewer();
	}

	return 0;
}

