/*
 * generate_gamma_delay.cpp
 *
 *  Created on: 25 июн. 2019 г.
 *      Author: vslavk
 */

#include "../hdr/generate_gamma_value.hpp"

generate_gamma_value::generate_gamma_value():generate_value()
{
	this->vf_lambda = 1;
	this->vf_alpha = 1;
};

generate_gamma_value::generate_gamma_value(double _lambda, double _alpha):generate_value()
{
	this->vf_lambda = _lambda;
	this->vf_alpha = _alpha;
};

generate_gamma_value::generate_gamma_value(long double _minvalue, double _probability, double _lambda, double _alpha):generate_value(_minvalue, _probability)
{
	this->vf_lambda = _lambda;
	this->vf_alpha = _alpha;
};

generate_gamma_value::~generate_gamma_value()
{

};


double generate_gamma_value::get_random_value()
{

	double ret_value = 1;

	if (this->vf_lambda <= 0 || this->vf_alpha <= 0)
	{
		exit(1);
	}

	if(this->vf_lambda < 1)
	{
		ret_value = first_method();
	}
	else
	{
		ret_value = second_method();
	}

	return ret_value;
};

//This function use method of gamma random number generation from book (rus.)
//"Справочник по вероятностным распределениям. (Вадзинский Р. Н.)", page 140
double generate_gamma_value::first_method(void)
{
	double ret_value = 0;

	bool label = true;

	while(label)
	{
		label = false;
		double rand_value1, rand_value2, rand_value3;
		rand_value1 = 0 + (0.000001 + rand() % 1000000) * ACCURANCY;
		rand_value2 = 0 + (0.000001 + rand() % 1000000) * ACCURANCY;
		rand_value3 = 0 + (0.000001 + rand() % 1000000) * ACCURANCY;

		double S1 = 0, S2 = 0;
		S1 = pow(rand_value1,(double)1/this->vf_lambda);
		S2 = pow(rand_value2,(double)1/(1-this->vf_alpha));
		fprintf(stdout, "r1 %lf\n", S1);
		fprintf(stdout, "r2 %lf\n", S2);
		if((S1+S2) <= 1)
		{
			ret_value = (S1*log(rand_value3))/(this->vf_alpha*(S1+S2));
			if(ret_value < 0 || ret_value > 20)
			{
				label = true;
			}
		}
		else
			label = true;
	}

	return ret_value;
};

//This function use method of gamma random number generation from book (rus.)
//"Александр Емельянов. Лаг-генераторы для моделирования рисковых ситуаций в системе Actor Pilgrim", page 112
double generate_gamma_value::second_method(void)
{
	double ret_value = 1;

	double lambda_int, lambda_frac;
	lambda_frac = modf(this->vf_lambda, &lambda_int);

	double integer_part = get_rand_int_value(lambda_int);
	double fractal_part = get_rand_frac_value(lambda_int, lambda_frac);

	//fprintf(stdout, "%f, %f\n", integer_part, fractal_part);
	ret_value = generate_value::vlf_minvalue + this->vf_alpha * (fractal_part + integer_part);

	return ret_value;
};

void generate_gamma_value::set_value_from_distr_config(distribution_config _distr_config)
{
	this->vf_probability = _distr_config.vf_probability;
	this->vlf_minvalue = _distr_config.vf_minvalue;
	this->vf_lambda = _distr_config.vf_u;
	this->vf_alpha = _distr_config.vf_v;
};

//This function is used to calculate internal part of random value for the second method
double generate_gamma_value::get_rand_int_value(double lambda_int)
{
	double rand_nums_summ = 0;
	for(int ix = 0; ix < lambda_int; ++ix)
	{
		double buf_rand = (0.000001 + rand() % 1000000) * ACCURANCY;
		buf_rand = -log(buf_rand);
		rand_nums_summ += buf_rand;
	}

	return rand_nums_summ;
};

//This function is used to calculate fractal part of random value for the second method
double generate_gamma_value::get_rand_frac_value(double lambda_int, double lambda_frac)
{
	double ret_value = 1;
	bool label = true;

	double buf_v = M_E/(M_E + lambda_frac);
	//fprintf(stdout, "b = %lf\n", buf_v);

	while(label)
	{
		double buf_a = 0, buf_b = 0;
		double rand_value1, rand_value2;
		label = false;

		rand_value1 = 0 + (0.000001 + rand() % 1000000) * ACCURANCY;
		rand_value2 = 0 + (0.000001 + rand() % 1000000) * ACCURANCY;

		if(rand_value1 <= buf_v)
		{
			buf_a = pow(rand_value1/buf_v, (double)1/lambda_frac);
			buf_b = rand_value2 * pow(buf_a, lambda_frac-1);
		}
		else
		{
			buf_a = 1 - log((rand_value1 - buf_v) / (1 - buf_v)); //pow(rand_value1/buf_b, 1/this->alpha);
			buf_b = rand_value2 * pow(M_E, -buf_a);
		}

		buf_b = buf_b - pow(buf_a, lambda_frac - 1) * pow(M_E, -buf_a);
		if(buf_b > 0)
		{
			label = true;
		}
		else
		{
			ret_value = buf_a;
		}
	}

	return ret_value;
};

