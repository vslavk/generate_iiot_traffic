/*
 * generate_veibull_delay.cpp
 *
 *  Created on: 26 июн. 2019 г.
 *      Author: vslavk
 */

#include "../hdr/generate_veibull_value.hpp"

generate_veibull_value::generate_veibull_value():generate_value()
{
	this->vf_lambda = 1;
	this->vf_alpha = 1;
};

generate_veibull_value::generate_veibull_value(double _lambda, double _alpha):generate_value()
{
	this->vf_lambda = _lambda;
	this->vf_alpha = _alpha;
};

generate_veibull_value::generate_veibull_value(long double _minvalue, double _probability, double _lambda, double _alpha):generate_value(_minvalue, _probability)
{
	this->vf_lambda = _lambda;
	this->vf_alpha = _alpha;
};

generate_veibull_value::~generate_veibull_value()
{

};

//This function use method of veibull random number generation from book (rus.)
//"Справочник по вероятностным распределениям. (Вадзинский Р. Н.)", page 158
double generate_veibull_value::get_random_value()
{
	double ret_value = 1;

	if (this->vf_lambda <= 0 || this->vf_alpha <= 0)
	{
		exit(1);
	}

	double buf_rand = (0.000001 + rand() % 1000000) * ACCURANCY;

	ret_value = this->vf_alpha * pow(-log(buf_rand), (double)1/this->vf_lambda);

	return ret_value;
};

void generate_veibull_value::set_value_from_distr_config(distribution_config _distr_config)
{
	this->vf_probability = _distr_config.vf_probability;
	this->vlf_minvalue = _distr_config.vf_minvalue;
	this->vf_lambda = _distr_config.vf_u;
	this->vf_alpha = _distr_config.vf_v;
};

