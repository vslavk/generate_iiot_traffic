/*
 * coap_sender.cpp
 *
 *  Created on: 9 июл. 2019 г.
 *      Author: vslavk
 */

#include "../hdr/coap_message_sender.hpp"

coap_message_sender::coap_message_sender(void) : message_sender()
{

};

coap_message_sender::coap_message_sender(std::string _message, const char* _host_addr, uint16_t _port) : message_sender(_message, _host_addr, _port)
{

};

coap_message_sender::~coap_message_sender(void)
{

};

int coap_message_sender::resolve_address(coap_address_t *dst)//const char *host, uint16_t port, coap_address_t *dst)
{

	struct addrinfo *res, *ainfo;
	struct addrinfo hints;
	int error, len=-1;

	memset(&hints, 0, sizeof(hints));
	memset(dst, 0, sizeof(*dst));
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_family = AF_UNSPEC;

	char service[5];
	sprintf(service, "%d", message_sender::vd_port);

	error = getaddrinfo(message_sender::vs_host_addr, service, &hints, &res);

	if (error != 0)
	{
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(error));
		return error;
	}

	for (ainfo = res; ainfo != NULL; ainfo = ainfo->ai_next)
	{
		switch (ainfo->ai_family)
		{
			case AF_INET6:
			case AF_INET:
				len = dst->size = ainfo->ai_addrlen;
				memcpy(&dst->addr.sin6, ainfo->ai_addr, dst->size);
				goto finish;
			default:
			;
		}
	}

	finish:
	freeaddrinfo(res);
	return len;
}

//On receive message event function
void coap_message_sender::onReceiveMessage(const coap_endpoint_t *local_interface, const coap_address_t *remote,
					 coap_pdu_t *sent, coap_pdu_t *received, const coap_tid_t id)
{
	coap_show_pdu(LOG_INFO, received);
}

//Method that sends coap get message
void coap_message_sender::send_message(void)
{
	coap_context_t  *ctx = NULL;
	coap_session_t *session = NULL;
	coap_address_t dst;
	coap_pdu_t *pdu = NULL;
	//int result = EXIT_FAILURE;;

	coap_startup();

	if (!(resolve_address(&dst) < 0))
	{
		ctx = coap_new_context(NULL);

		if (ctx && (session = coap_new_client_session(ctx, NULL, &dst,
		                                                  COAP_PROTO_UDP)))
		{
			coap_register_response_handler(ctx, (coap_response_handler_t)onReceiveMessage);

			pdu = coap_pdu_init(COAP_MESSAGE_CON,
					             COAP_REQUEST_GET,
					             0 /* message id */,
					             coap_session_max_pdu_size(session));
			if (pdu)
			{
				coap_add_option(pdu, COAP_OPTION_URI_PATH, message_sender::vs_message.size(), (const unsigned char*)message_sender::vs_message.c_str());

				coap_send(session, pdu);

				coap_io_process(ctx, 0);

				//result = EXIT_SUCCESS;
			}
			else
			{
				coap_log( LOG_EMERG, "cannot create PDU\n" );
			}
		}
		else
		{
			coap_log(LOG_EMERG, "cannot create client session\n");
		}
	}
	else
	{
		coap_log(LOG_CRIT, "failed to resolve address\n");
	}


	coap_session_release(session);
	coap_free_context(ctx);
	coap_cleanup();
}



