/*
 * internal_functions.cpp
 *
 *  Created on: 18 июл. 2019 г.
 *      Author: vslavk
 */

#include "../hdr/internal_functions.hpp"


//_input_type:
	//	read configuration from file or from incoming messages
//_input_var:
	//	the name of file with configurations in case when program read configurations from file
	//	the interface address in case when program read configurations from incoming messages
int internal_functions::read_command_line_arguments(int _argc, char **_argv, size_t* _input_type, std::string* _input_var, std::string* _port)
{
	int ix = 1;
	int status = 0;

	while (ix < _argc)
	{
		if( !strcmp((const char*)_argv[ix], "-t") || !strcmp((const char*)_argv[ix], "type") )
		{
			if( !strcmp((const char*)_argv[ix+1], "file") )
			{
				*_input_type = 1;
				ix = ix +2;
			}
			else if ( !strcmp((const char*)_argv[ix+1], "net") )
			{
				*_input_type = 2;
				ix = ix +2;
			}
			else
			{
				status = 2;
				break;
			}
		}
		else if ( !strcmp((const char*)_argv[ix], "-f") || !strcmp((const char*)_argv[ix], "filename") )
		{
			if(*_input_type == 1)
			{
				*_input_var = _argv[ix+1];
				ix = ix +2;
			}
			else
			{
				status = 3;
				break;
			}
		}
		else if ( !strcmp((const char*)_argv[ix], "-h") || !strcmp((const char*)_argv[ix], "host") )
		{
			if(*_input_type == 2)
			{
				*_input_var = _argv[ix+1];
				ix = ix +2;
			}
			else
			{
				status = 3;
				break;
			}
		}
		else if ( !strcmp((const char*)_argv[ix], "-p") || !strcmp((const char*)_argv[ix], "port") )
		{
			if(*_input_type == 2)
			{
				*_port = _argv[ix+1];
				ix = ix +2;
			}
			else
			{
				status = 3;
				break;
			}
		}
	}
	if( *_input_type == 1  && !strcmp(_input_var->c_str(), "NULL") )
	{
		status = 4;
	}
	else if( *_input_type == 2  && (!strcmp(_input_var->c_str(), "NULL") || !strcmp(_port->c_str(), "NULL")))
	{
		status = 5;
	}

	return status;
}

std::vector<generate_value*> internal_functions::generate_random_distr_values(std::vector<distribution_config> _distr_list)
{
	std::vector<generate_value*> generator_arr;

	for(auto ix = _distr_list.begin(); ix < _distr_list.end(); ++ix)
	{
		generate_value* buf;

		//select child-type for generate_value object
		if(!strcmp(ix->ps_type, "uniform"))
		{
			buf = new generate_uniform_value(ix->vf_minvalue, ix->vf_probability, ix->vf_u, ix->vf_v);
		}
		else if(!strcmp(ix->ps_type,"exponential") || !strcmp(ix->ps_type,"exp"))
		{
			buf = new generate_exp_value(ix->vf_minvalue, ix->vf_probability, ix->vf_u);
		}
		else if(!strcmp(ix->ps_type, "gamma"))
		{
			buf = new generate_gamma_value(ix->vf_minvalue, ix->vf_probability, ix->vf_u, ix->vf_v);
		}
		else if(!strcmp(ix->ps_type, "erlang"))
		{
			buf = new generate_erlang_value(ix->vf_minvalue, ix->vf_probability, ix->vf_u, ix->vf_v);
		}
		else if(!strcmp(ix->ps_type, "beta"))
		{
			buf = new generate_beta_value(ix->vf_minvalue, ix->vf_probability, ix->vf_u, ix->vf_v);
		}
		else if(!strcmp(ix->ps_type, "veibull"))
		{
			buf = new generate_veibull_value(ix->vf_minvalue, ix->vf_probability, ix->vf_u, ix->vf_v);
		}
		else if(!strcmp(ix->ps_type, "const"))
		{
			buf = new generate_const_value(ix->vf_minvalue, ix->vf_probability);
		}
		else
		{
			buf = new generate_const_value(ix->vf_minvalue, ix->vf_probability);
		}
		generator_arr.push_back(buf);
	}
	return generator_arr;
};

std::string internal_functions::generate_random_string(size_t _size)
{
	std::string ret_string = "";

	for(size_t ix = 0; ix < _size; ++ix)
	{
		double random_value = 96 + (26 * (rand()/(RAND_MAX + 1.0)));
		char buf_symb = (char)random_value;
		ret_string.push_back(buf_symb);
	}

	return ret_string;
};

void internal_functions::print_rand_values_arr(std::vector<long double> _rand_values_array, size_t _message_quantity)
{

	for (size_t ix = 0; ix < _message_quantity; ++ix)
	{
		if (ix%10 == 0 && ix != 0)
		{
			fprintf(stdout, "\n");
		}
		else if (ix != 0)
		{
			fprintf(stdout, ",");
		}

		fprintf(stdout, "%f", (double)_rand_values_array[ix]);
	}
};

std::vector<long double> internal_functions::get_rand_values_array(std::vector<generate_value*> _generator_arr, size_t _message_quantity)
{
	std::vector<long double> rand_values_array;
	for (size_t ix = 0; ix < _message_quantity; ++ix)
	{
		double buf_probability = 0;
		double random_probability =  (0.000001 + rand() % 1000000) * ACCURANCY;
		//fprintf(stdout, "%d:%f\n", ix, random_probability);
		for(size_t iy = 0; iy < _generator_arr.size(); ++iy)
		{
			buf_probability = buf_probability + _generator_arr[iy]->get_probability();

			if((random_probability <= buf_probability) || (buf_probability >= 1))
			{
				rand_values_array.push_back(_generator_arr[iy]->get_random_value());
				break;
			}
		}
	}

	return rand_values_array;
};

void internal_functions::start_message_sender(size_t _app_layer_type, std::vector<long double> _delay_arr, std::vector<std::string> _message, bool *_label,
														const char* _host_addr, uint16_t _port)
{
	std::unique_ptr<message_sender> msender;

	//select child-type for message_sender object
	if(_app_layer_type == 1)
	{
		msender = std::make_unique<coap_message_sender>(" ", _host_addr, _port);
	}
	else if(_app_layer_type == 2)
	{
		msender = std::make_unique<coap_message_sender>(" ", _host_addr, _port);
	}
	else if(_app_layer_type == 3)
	{
		msender = std::make_unique<coap_message_sender>(" ", _host_addr, _port);
	}
	else if(_app_layer_type == 4)
	{
		msender = std::make_unique<coap_message_sender>(" ", _host_addr, _port);
	}
	else if(_app_layer_type == 5)
	{
		msender = std::make_unique<coap_message_sender>(" ", _host_addr, _port);
	}
	else
	{
		msender = std::make_unique<coap_message_sender>(" ", _host_addr, _port);
	}

	for(size_t ix = 0; ix < _delay_arr.size(); ++ix)
	{
		msender->set_message(_message[ix]);

		msender->send_message();

		usleep(_delay_arr[ix]);
	}

	*_label = true;
};

void internal_functions::configure_message_sender(std::unique_ptr<read_configuration> _distr_config, bool _network_mode)
{
	std::vector<std::string> messages;
	std::vector<std::unique_ptr<std::thread>> thread_pool;
	size_t app_layer = _distr_config->get_application_layer();
	size_t messages_quantity = _distr_config->get_messages_quantity();
	size_t threads_quantity = _distr_config->get_threads_quantity();

	std::string dst_host = _distr_config->get_dst_host_name();
	uint16_t dst_port = _distr_config->get_dst_port_name();

	bool label = true;

	bool stop_labels[threads_quantity] = {false};

	std::vector<distribution_config> distrib_config_list = _distr_config->get_distribution_config_list(),
									 msize_config_list = _distr_config->get_message_size_config_list();

	std::vector<generate_value*> generator_arr = internal_functions::generate_random_distr_values(distrib_config_list),
								 message_size_arr = internal_functions::generate_random_distr_values(msize_config_list);

	std::vector<long double> thread_rand_delay_arr,
							  message_size_rand_arr;
	for(size_t ix = 0; ix < threads_quantity; ++ix)
	{
		std::vector<long double> thread_values_array = internal_functions::get_rand_values_array(generator_arr, messages_quantity);
		std::vector<long double> mes_size_values_array = internal_functions::get_rand_values_array(message_size_arr, messages_quantity);

		thread_rand_delay_arr.insert(thread_rand_delay_arr.end(), thread_values_array.begin(), thread_values_array.end());
		message_size_rand_arr.insert(message_size_rand_arr.end(), mes_size_values_array.begin(), mes_size_values_array.end());

		//fprintf(stdout, "Threads\n");
		//internal_functions::print_rand_values_arr(thread_values_array, messages_quantity);
		//fprintf(stdout, "Message_size\n");
		//internal_functions::print_rand_values_arr(mes_size_values_array, messages_quantity);
	}

	for(size_t ix = 0; ix < thread_rand_delay_arr.size(); ++ix)
	{
		thread_rand_delay_arr[ix] = thread_rand_delay_arr[ix] * 1000000;
	}

	fprintf(stdout, "Random string generation was started!\n");
	for(size_t ix = 0; ix < message_size_rand_arr.size(); ++ix)
	{
		size_t buf_size;
		buf_size = (size_t)(1 + message_size_rand_arr[ix] * 1000);
		messages.push_back(internal_functions::generate_random_string(buf_size));
	}
	fprintf(stdout, "Random string generation was finished!\n");

	fprintf(stdout, "----------------------------------------\n");

	fprintf(stdout, "Starting threads...!\n");
	for(size_t ix = 0; ix < threads_quantity; ++ix)
	{
		thread_pool.push_back( std::make_unique<std::thread>(
				internal_functions::start_message_sender,
				app_layer,
				std::vector<long double>(
						thread_rand_delay_arr.begin() + ix * messages_quantity,
						thread_rand_delay_arr.begin() + (ix + 1) * messages_quantity
				),
				std::vector<std::string>(
						messages.begin() + ix * messages_quantity,
						messages.begin() + (ix + 1) * messages_quantity
				),
				&(stop_labels[ix]),
				dst_host.c_str(),
				dst_port
		));
		thread_pool[ix]->detach();
	}
	fprintf(stdout, "All threads were started!\n");

	//wait before every thread were stopped
	while (!_network_mode && label)
	{
		bool buf = stop_labels[0];

		for(size_t ix = 1; ix < threads_quantity; ++ix)
		{
			buf = buf & stop_labels[ix];
		}

		if (buf)
		{
			label = false;
		}
	}
	if(!_network_mode)
	{
		fprintf(stdout, "Thread`s work were finished!\n");
	}

	for(int ix = generator_arr.size()-1; ix > -1; ix--)
	{
		delete(generator_arr[ix]);
	}
	for(int ix = message_size_arr.size()-1; ix > -1; ix--)
	{
		delete(message_size_arr[ix]);
	}
};
