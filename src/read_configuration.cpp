/*
 * read_configuration.cpp
 *
 *  Created on: 12 июл. 2019 г.
 *      Author: vslavk
 */
#include "../hdr/read_configuration.hpp"

read_configuration::read_configuration(void)
{
	this->vs_filename = "config.json";
	this->vd_application_layer = 0;
	this->vs_filetext = "";
	this->vd_messages_quantity = 0;
	this->vd_threads_quantity = 0;
	this->vs_dst_host = "127.0.0.1";
	this->vd_dst_port = 9999;
};

read_configuration::read_configuration(std::string _filename)
{
	this->vs_filename = _filename;
	this->vd_application_layer = 0;
	this->vs_filetext = "";

	read_config_file();
};

read_configuration::~read_configuration(void)
{

};

void read_configuration::set_text(const char* _text)
{
	this->vs_filetext = _text;
};

//Method that used to parse JSON configuration file/string
void read_configuration::parse_json_message(void)
{
	rapidjson::Document document;
	document.Parse(this->vs_filetext.c_str());

	assert(document.HasMember("dst_host"));
	assert(document["dst_host"].IsString());
	this->vs_dst_host = document["dst_host"].GetString();

	assert(document["dst_port"].IsInt());
	this->vd_dst_port = document["dst_port"].GetInt();

	assert(document.HasMember("protocol"));
	assert(document["protocol"].IsString());
	const char* app_label = document["protocol"].GetString();

	if(!strcmp(app_label, "udp"))
	{
		this->vd_application_layer = 1;
	}
	else if(!strcmp(app_label, "tcp"))
	{
		this->vd_application_layer = 2;
	}
	else if(!strcmp(app_label, "coap"))
	{
		this->vd_application_layer = 3;
	}
	else if(!strcmp(app_label, "mqtt"))
	{
		this->vd_application_layer = 4;
	}
	else if(!strcmp(app_label, "modbusip"))
	{
		this->vd_application_layer = 5;
	}
	else
	{
		this->vd_application_layer = 3;
	}

	assert(document["mes_quantity"].IsInt());
	this->vd_messages_quantity = document["mes_quantity"].GetInt();

	if(this->vd_messages_quantity < 1)
	{
		return;
	}

	assert(document["threads_quantity"].IsInt());
	this->vd_threads_quantity = document["threads_quantity"].GetInt();

	if(this->vd_threads_quantity < 1)
	{
		return;
	}

	const rapidjson::Value& delay_distrib = document["distributions"];
	assert(delay_distrib.IsArray());
	this->vo_distrib_configs = parse_json_distrib(delay_distrib);

	const rapidjson::Value& message_size = document["messages_size"];
	assert(message_size.IsArray());
	this->vo_message_size_configs = parse_json_distrib(message_size);
};

//Method that used to parse distribution structure for delay and message size distributions
std::vector<distribution_config> read_configuration::parse_json_distrib(const rapidjson::Value& _distrib)
{
	std::vector<distribution_config> ret_array;

	for(rapidjson::SizeType ix = 0; ix < _distrib.Size(); ix++)
	{
		distribution_config buf;
		if(		_distrib[ix].FindMember("probability") != _distrib[ix].MemberEnd()
				&& _distrib[ix].FindMember("type") != _distrib[ix].MemberEnd()
				&& _distrib[ix].FindMember("min_value") != _distrib[ix].MemberEnd())
		{
			buf.vf_probability = _distrib[ix]["probability"].GetDouble();
			buf.ps_type = (char*)_distrib[ix]["type"].GetString();
			buf.vf_minvalue = _distrib[ix]["min_value"].GetDouble();
		}
		else
			continue;

		if(_distrib[ix].FindMember("u") != _distrib[ix].MemberEnd())
		{
			buf.vf_u = _distrib[ix]["u"].GetDouble();
		}
		else if (strcmp(buf.ps_type,"const") != false)
		{
			continue;
		}

		if(_distrib[ix].FindMember("v") != _distrib[ix].MemberEnd())
		{
			buf.vf_v = _distrib[ix]["v"].GetDouble();
		}
		else if ((	strcmp(buf.ps_type,"exp") != false)
					&& (strcmp(buf.ps_type,"exponential") != false)
					&& (strcmp(buf.ps_type,"const") != false))
		{
			continue;
		}

		ret_array.push_back(buf);
	}

	return ret_array;
};

//Method that used to read configuration file and write it to vs_filetext variable
void read_configuration::read_config_file(void)
{
	FILE *fp;

	fp = fopen(this->vs_filename.c_str(), "r");

	if (fp ==NULL)
	{
		fprintf(stderr, "Cannot open file.\n");
		exit (1);
	}

	char ch = ' ';
	while((ch = fgetc(fp)) != EOF)
	{
		if( (ch != '\t') && (ch != '\n') )
		{
			this->vs_filetext.push_back(ch);
		}
	}

	fclose(fp);

	parse_json_message();
};

//Some get methods
std::vector<distribution_config> read_configuration::get_distribution_config_list(void)
{
	return this->vo_distrib_configs;
};

std::vector<distribution_config> read_configuration::get_message_size_config_list(void)
{
	return this->vo_message_size_configs;
};

int read_configuration::get_application_layer(void)
{
	return this->vd_application_layer;
};

int read_configuration::get_messages_quantity(void)
{
	return this->vd_messages_quantity;
};

int read_configuration::get_threads_quantity(void)
{
	return this->vd_threads_quantity;
};

std::string read_configuration::get_dst_host_name(void)
{
	return this->vs_dst_host;
};

uint16_t read_configuration::get_dst_port_name(void)
{
	return this->vd_dst_port;
};
