/*
 * message_sender.cpp
 *
 *  Created on: 18 июл. 2019 г.
 *      Author: vslavk
 */

#include "../hdr/message_sender.hpp"

message_sender::message_sender(void):
	vs_message(" "), vs_host_addr("0.0.0.0"), vd_port(9999)
{

};

message_sender::message_sender(std::string _message, const char* _host_addr, uint16_t _port):
		vs_message(_message), vs_host_addr(_host_addr), vd_port(_port)
{

};

message_sender::~message_sender(void)
{

};

//void message_sender::send_message(void)
//{

//};

void message_sender::set_message(std::string _message)
{
	this->vs_message = _message;
};
