/*
 * generate_beta_delay.cpp
 *
 *  Created on: 26 июн. 2019 г.
 *      Author: vslavk
 */

#include "../hdr/generate_beta_value.hpp"

generate_beta_value::generate_beta_value(void):generate_value()
{
	this->vf_u = 1;
	this->vf_v = 1;
	calculate_k_h_for_beta_generator();
};

generate_beta_value::generate_beta_value(double _u, double _v):generate_value()
{
	this->vf_u = _u;
	this->vf_v = _v;
	calculate_k_h_for_beta_generator();
};

generate_beta_value::generate_beta_value(long double _minvalue, double _probability, double _u, double _v):generate_value(_minvalue, _probability)
{
	this->vf_u = _u;
	this->vf_v = _v;
	calculate_k_h_for_beta_generator();
};

generate_beta_value::~generate_beta_value(void)
{

};

double generate_beta_value::get_random_value(void)
{
	double ret_value = 1;

	if(this->vf_u <= 0 || this->vf_v <= 0)
	{
		exit(1);
	}

	if(this->vf_u > 1 && this->vf_v > 1)
	{
		ret_value = second_method();
	}
	else
	{
		ret_value = first_yonka_method();
	}

	return ret_value;
};

//This function use first method of beta random number generation from book (rus.)
//"Справочник по вероятностным распределениям. (Вадзинский Р. Н.)", pages 216-217
double generate_beta_value::first_yonka_method(void)
{
	double ret_value = 0;

	long double _vd_u = this->vf_u;
	long double _vd_v = this->vf_v;

	bool label = true;
	while(label)
	{
		label = false;
		double buf_rand1 = (0.000001 + rand() % 1000000) * ACCURANCY;
		double buf_rand2 = (0.000001 + rand() % 1000000) * ACCURANCY;

		double S1 = 0, S2 = 0;
		S1 = pow(buf_rand1, (double)1/_vd_u);
		S2 = pow(buf_rand2, (double)1/_vd_v);

		if((S1+S2)<=1)
		{
			ret_value = S1/(S1 + S2);
			if(ret_value < 0 || ret_value > 20)
			{
				label = true;
			}
		}
		else
			label = true;
	}

	return ret_value;
};

//This function use second method of beta random number generation from book (rus.)
//"Справочник по вероятностным распределениям. (Вадзинский Р. Н.)", pages 216-217
double generate_beta_value::second_method(void)
{
	double ret_value = 1;

	bool label = true;

	while(label)
	{
		double buf_rand1 = (0.000001 + rand() % 1000000) * ACCURANCY;
		double buf_rand2 = (0.000001 + rand() % 1000000) * ACCURANCY;

		double buf_epsilon = buf_rand1;
		double buf_nu = buf_rand2 * this->vf_h;

		double buf_y = this->vf_k * pow(buf_epsilon, this->vf_u-1) * pow(1 - buf_epsilon, this->vf_v-1);
		if(buf_nu <= buf_y)
		{
			ret_value = buf_epsilon;
			label = false;
		}
	}

	return ret_value;
};

void generate_beta_value::set_value_from_distr_config(distribution_config _distr_config)
{
	this->vf_probability = _distr_config.vf_probability;
	this->vlf_minvalue = _distr_config.vf_minvalue;
	this->vf_u = _distr_config.vf_u;
	this->vf_v = _distr_config.vf_v;
};

//This function calculate vf_k and vf_h variables by long numbers precision libraries
//vf_k and vf_h by formulas which takes form this book (rus):
//"Справочник по вероятностным распределениям. (Вадзинский Р. Н.)", pages 217
void generate_beta_value::calculate_k_h_for_beta_generator(void)
{

	mpfr_t g_u, g_v, g_uv;
	mpfr_init2 (g_u, 500);
	mpfr_init2 (g_v, 500);
	mpfr_init2 (g_uv, 500);

	mpfr_set_d (g_u, this->vf_u, MPFR_RNDD);
	mpfr_gamma (g_u, g_u, MPFR_RNDU);

	mpfr_set_d (g_v, this->vf_v, MPFR_RNDD);
	mpfr_gamma (g_v, g_v, MPFR_RNDU);

	mpfr_set_d (g_uv, this->vf_u + this->vf_v, MPFR_RNDD);
	mpfr_gamma (g_uv, g_uv, MPFR_RNDU);

	mpfr_mul(g_u, g_u, g_v, MPFR_RNDU);
	mpfr_div(g_uv, g_uv, g_u, MPFR_RNDU);

	this->vf_k = mpfr_get_d(g_uv, MPFR_RNDD);

	mpfr_set_d (g_u, this->vf_u - 1, MPFR_RNDD);
	mpfr_set_d (g_v, this->vf_v - 1, MPFR_RNDD);

	mpfr_pow(g_u, g_u, g_u, MPFR_RNDU);
	mpfr_pow(g_v, g_v, g_v, MPFR_RNDU);
	mpfr_mul(g_u, g_u, g_v, MPFR_RNDU);
	mpfr_mul(g_uv, g_uv, g_u, MPFR_RNDU);

	mpfr_set_d (g_u, this->vf_u + this->vf_v - 2, MPFR_RNDD);
	mpfr_pow(g_u, g_u, g_u, MPFR_RNDU);

	mpfr_div (g_uv, g_uv, g_u, MPFR_RNDU);

	this->vf_h = mpfr_get_d(g_uv, MPFR_RNDD);

	mpfr_clear (g_u);
	mpfr_clear (g_v);
	mpfr_clear (g_uv);
	mpfr_free_cache ();
}
