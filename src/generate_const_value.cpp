/*
 * generate_const_value.cpp
 *
 *  Created on: 30 июл. 2019 г.
 *      Author: vslavk
 */

#include "../hdr/generate_const_value.hpp"

generate_const_value::generate_const_value():generate_value()
{ };

generate_const_value::generate_const_value(long double _minvalue, double _probability):generate_value(_minvalue, _probability)
{ };

generate_const_value::~generate_const_value()
{ };

double generate_const_value::get_random_value()
{
	if (this->vlf_minvalue < 0 && this->vf_probability > 1.0)
	{
		exit(1);
	}

	return this->vlf_minvalue;
};

void generate_const_value::set_value_from_distr_config(distribution_config _distr_config)
{
	this->vf_probability = _distr_config.vf_probability;
	this->vlf_minvalue = _distr_config.vf_minvalue;
};




