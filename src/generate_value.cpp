/*
 * generate_delay.cpp
 *
 *  Created on: 25 июн. 2019 г.
 *      Author: vslavk
 */

#include "../hdr/generate_value.hpp"

generate_value::generate_value(void)
{
	this->vf_probability = 1;
	this->vlf_minvalue = 0;
};

generate_value::generate_value(double _minvalue, double _probability)
{
	this->vf_probability = _probability;
	this->vlf_minvalue = _minvalue;
};

generate_value::~generate_value(void)
{

};

/*void generate_value::set_value_from_distr_config(distribution_config _distr_config)
{
	this->vf_probability = _distr_config.vf_probability;
	this->vlf_minvalue = _distr_config.vf_minvalue;
};

double generate_value::get_random_value(void)
{
	return this->vlf_minvalue;
};*/

double generate_value::get_probability(void)
{
	return this->vf_probability;
};
