#include "../hdr/configuration_server.hpp"

configuration_server::configuration_server(const char* _host, const char* _port)
{
    listener = socket(AF_INET, SOCK_STREAM, 0);
    if(listener < 0)
    {
        perror("socket");
        exit(1);
    }

    addr.sin_family = AF_INET;
    addr.sin_port = htons(atoi(_port));
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    if(bind(listener, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        perror("bind");
        exit(2);
    }
};

void configuration_server::serve_forewer(void)
{
	std::unique_ptr<read_configuration> po_config;
	char payload[65536];
    listen(listener, 1);

    while(true)
    {
        int sock = accept(listener, NULL, NULL);
        if(sock < 0)
        {
            perror("accept");
            exit(3);
        }

        while(true)
        {
            int bytes_read = recv(sock, payload, 65536, 0);
            if(bytes_read <= 0) break;

            po_config->set_text(payload);
            po_config->parse_json_message();
            internal_functions::configure_message_sender(std::move(po_config), true);

            send(sock, "OK", 3, 0);
        }

        close(sock);
    }
};
