/*
 * generate_uniform_delay.cpp
 *
 *  Created on: 26 июн. 2019 г.
 *      Author: vslavk
 */

#include "../hdr/generate_uniform_value.hpp"

generate_uniform_value::generate_uniform_value():generate_value()
{
	this->vf_x = 1;
	this->vf_y = 1;
};

generate_uniform_value::generate_uniform_value(double _x, double _y):generate_value()
{
	this->vf_x = _x;
	this->vf_y = _y;
};

generate_uniform_value::generate_uniform_value(long double _minvalue, double _probability, double _x, double _y):generate_value(_minvalue, _probability)
{
	this->vf_x = _x;
	this->vf_y = _y;
};

generate_uniform_value::~generate_uniform_value()
{

};

//This function use method of uniform random number generation from book (rus.)
//"Справочник по вероятностным распределениям. (Вадзинский Р. Н.)", page 211
double generate_uniform_value::get_random_value()
{
	double ret_value = 1;
	if ((this->vf_x > this->vf_y) || (this->vf_y < 0) || (this->vf_x < 0))
	{
		exit(1);
	}

	double buf_rand = (0.000001 + rand() % 1000000) * ACCURANCY;

	ret_value = this->vf_x +(this->vf_y - this->vf_x) * buf_rand;

	return ret_value;
};

void generate_uniform_value::set_value_from_distr_config(distribution_config _distr_config)
{
	this->vf_probability = _distr_config.vf_probability;
	this->vlf_minvalue = _distr_config.vf_minvalue;
	this->vf_x = _distr_config.vf_u;
	this->vf_y = _distr_config.vf_v;
};

