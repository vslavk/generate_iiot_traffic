/*
 * generate_exp_delay.cpp
 *
 *  Created on: 25 июн. 2019 г.
 *      Author: vslavk
 */


#include "../hdr/generate_exp_value.hpp"

generate_exp_value::generate_exp_value():generate_value()
{
	this->vf_lambda = 1;
};

generate_exp_value::generate_exp_value(double _lambda):
		generate_value()
{
	this->vf_lambda = _lambda;
};

generate_exp_value::generate_exp_value(long double _minvalue, double _probability, double _lambda):
		generate_value(_minvalue, _probability)
{
	this->vf_lambda = _lambda;
};

generate_exp_value::~generate_exp_value()
{

};

//This function use method of exponential random number generation from book (rus.)
//"Справочник по вероятностным распределениям. (Вадзинский Р. Н.)", page 136
double generate_exp_value::get_random_value()
{
	double ret_value = 1;

	if (this->vf_lambda <= 0)
	{
		exit(1);
	}

	ret_value = (0.000001 + rand() % 1000000) * ACCURANCY;
	ret_value = generate_value::vlf_minvalue + (-log(ret_value)/this->vf_lambda);

	return ret_value;
};

void generate_exp_value::set_value_from_distr_config(distribution_config _distr_config)
{
	this->vf_probability = _distr_config.vf_probability;
	this->vlf_minvalue = _distr_config.vf_minvalue;
	this->vf_lambda = _distr_config.vf_u;
};
