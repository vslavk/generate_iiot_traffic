/*
 * generate_erlang_delay.cpp
 *
 *  Created on: 26 июн. 2019 г.
 *      Author: vslavk
 */

#include "../hdr/generate_erlang_value.hpp"

generate_erlang_value::generate_erlang_value():generate_value()
{
	this->vf_m = 1;
	this->vf_alpha = 1;
};

generate_erlang_value::generate_erlang_value(int _m, double _alpha):generate_value()
{
	this->vf_m = _m;
	this->vf_alpha = _alpha;
};

generate_erlang_value::generate_erlang_value(long double _minvalue, double _probability, int _m, double _alpha):generate_value(_minvalue, _probability)
{
	this->vf_m = _m;
	this->vf_alpha = _alpha;
};

generate_erlang_value::~generate_erlang_value()
{

};

//This function use method of erlang random number generation from book (rus.)
//"Справочник по вероятностным распределениям. (Вадзинский Р. Н.)", page 147
double generate_erlang_value::get_random_value()
{
	double ret_value = 1;
	double mul_randomValue = 1;

	if (this->vf_m <= 0 || this->vf_alpha <= 0)
	{
		exit(1);
	}

	for(int ix = 0; ix < this->vf_m; ++ix)
	{
		double buf_rand = (0.000001 + rand() % 1000000) * ACCURANCY;
		mul_randomValue = mul_randomValue * buf_rand;
	}

	ret_value = -log(mul_randomValue)/(this->vf_alpha);

	return ret_value;
};

void generate_erlang_value::set_value_from_distr_config(distribution_config _distr_config)
{
	this->vf_probability = _distr_config.vf_probability;
	this->vlf_minvalue = _distr_config.vf_minvalue;
	this->vf_alpha = _distr_config.vf_u;
	this->vf_m = _distr_config.vf_v;
};
