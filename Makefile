CC=g++
CFLAGS=-c -Wall
PATH_TO_GSL=/usr/include/gsl
PATH_TO_COAP=/usr/local/include/coap2
INCLUDE_LIBS=-lm -lpthread -lmpfr -lgmp -lcoap-2-openssl
INCLUDE_PATH=-I $(PATH_TO_GSL) -I $(PATH_TO_COAP) 
LDFLAGS=
SOURCES=main.cpp src/read_configuration.cpp src/message_sender.cpp src/coap_message_sender.cpp src/generate_value.cpp src/generate_const_value.cpp src/generate_uniform_value.cpp src/generate_gamma_value.cpp src/generate_exp_value.cpp src/generate_beta_value.cpp src/generate_erlang_value.cpp src/generate_veibull_value.cpp src/internal_functions.cpp src/configuration_server.cpp
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=iiot-gen

all: $(SOURCES) $(EXECUTABLE)
	
$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(LDFLAGS) $(OBJECTS) $(INCLUDE_PATH) $(INCLUDE_LIBS) -o $@ 
.cpp.o:
	$(CC) $(CFLAGS)  $< -o $@ 
clean:
	rm -f src/*.o *.o
delete:
	rm -f $(EXECUTABLE)
