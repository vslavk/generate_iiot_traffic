/*
 * message_senders.hpp
 *
 *  Created on: 18 июл. 2019 г.
 *      Author: vslavk
 */

#ifndef HDR_MESSAGE_SENDERS_HPP_
#define HDR_MESSAGE_SENDERS_HPP_

//Header which include all exist message sender classes
#include "coap_message_sender.hpp"
#include "message_sender.hpp"

#endif /* HDR_MESSAGE_SENDERS_HPP_ */
