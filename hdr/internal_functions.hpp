/*
 * internal_functions.hpp
 *
 *  Created on: 18 июл. 2019 г.
 *      Author: vslavk
 */

#ifndef HDR_INTERNAL_FUNCTIONS_HPP_
#define HDR_INTERNAL_FUNCTIONS_HPP_

#include "libs.hpp"
#include "generate_random_values.hpp"
#include "message_senders.hpp"
#include "read_configuration.hpp"

namespace internal_functions
{
	//Function thar used to read command line arguments. Examples:
	//	./<program_name> -t file -f <path/to/configuration file>
	//	./<program_name> -t net -h 127.0.0.1 -p 9898
	int read_command_line_arguments(int _argc, char **_argv, size_t* _input_type, std::string* _input_var, std::string* _port);

	//Create generators list by exists distribution configuration list
	std::vector<generate_value*> generate_random_distr_values(std::vector<distribution_config> _distr_list);

	//Random string generator
	std::string generate_random_string(size_t _size);

	//Get random variables list by distribution generators list
	std::vector<long double> get_rand_values_array(std::vector<generate_value*> _generator_arr, size_t _message_quantity);

	//Debug function to print random values to stdout
	void print_rand_values_arr(std::vector<long double> _rand_values_array, size_t _message_quantity);

	//Start message sender
	void start_message_sender(size_t _app_layer_type, std::vector<long double> _delay_arr, std::vector<std::string> _message, bool *_label,
																	const char* _host_addr = "0.0.0.0", uint16_t _port = 9999 );

	//Function that used to configure message sender before start traffic generation
	void configure_message_sender(std::unique_ptr<read_configuration> _distr_config, bool _network_mode = false);
}

#endif /* HDR_INTERNAL_FUNCTIONS_HPP_ */
