/*
 * generate_exp_delay.hpp
 *
 *  Created on: 25 июн. 2019 г.
 *      Author: vslavk
 */

#ifndef HDR_GENERATE_EXP_VALUE_HPP_
#define HDR_GENERATE_EXP_VALUE_HPP_

#include "generate_value.hpp"

//Child class for generate_value class that realize random generator by exponential distribution model
class generate_exp_value : public generate_value
{
public:
	generate_exp_value(void);
	generate_exp_value(double _lambda);
	generate_exp_value(long double _minvalue, double _probability, double _lambda);
	~generate_exp_value(void);

	double get_random_value(void);
	void set_value_from_distr_config(distribution_config _distr_config);
private:
	double vf_lambda;
};

#endif /* HDR_GENERATE_EXP_VALUE_HPP_ */
