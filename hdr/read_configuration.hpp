/*
 * read_configuration.hpp
 *
 *  Created on: 12 июл. 2019 г.
 *      Author: vslavk
 */

#ifndef HDR_READ_CONFIGURATION_HPP_
#define HDR_READ_CONFIGURATION_HPP_

#include "libs.hpp"

//Structure with distribution configuration
struct distribution_config
{
	double vf_probability;
	char* ps_type;
	long double vf_minvalue;
	long double vf_u;
	long double vf_v;
};

//Class that using to read configuration form JSON file/string
class read_configuration
{
public:
	read_configuration(void);
	read_configuration(std::string _filename);
	~read_configuration(void);

	//Some get methods
	std::vector<distribution_config> get_distribution_config_list(void);
	std::vector<distribution_config> get_message_size_config_list(void);
	int get_application_layer(void);
	int get_messages_quantity(void);
	int get_threads_quantity(void);

	std::string get_dst_host_name(void);
	uint16_t get_dst_port_name(void);

	void set_text(const char* _text);
	void parse_json_message(void);
private:
	void read_config_file(void);
	void start_server(void);
	std::vector<distribution_config> parse_json_distrib(const rapidjson::Value& _distrib);

	//File name variable
	std::string vs_filename;
	//Variable which contained JSON configuration string
	std::string vs_filetext;

	//Destination host and port variables
	std::string vs_dst_host;
	uint16_t vd_dst_port;

	//Type of the application layer that used by message sender
	int vd_application_layer;
	//Threads quantity variable
	int vd_threads_quantity;
	//Message quantity variables
	int vd_messages_quantity;

	//distribution configuration list for:
	//	message send delay distribution
	//	message size distribution
	std::vector<distribution_config> vo_distrib_configs;
	std::vector<distribution_config> vo_message_size_configs;
};


#endif /* HDR_READ_CONFIGURATION_HPP_ */
