/*
 * message_sender.hpp
 *
 *  Created on: 18 июл. 2019 г.
 *      Author: vslavk
 */

#ifndef HDR_MESSAGE_SENDER_HPP_
#define HDR_MESSAGE_SENDER_HPP_

#include "libs.hpp"

//Parent class for message sender objects
class message_sender
{
public:
	message_sender(void);
	message_sender(std::string _message, const char* _host_addr = "0.0.0.0", uint16_t _port = 9999);
	virtual ~message_sender(void);

	virtual void send_message(void) = 0;
	void set_message(std::string _message);
protected:
	std::string vs_message;
	const char* vs_host_addr;
	uint16_t vd_port;
};

#endif /* HDR_MESSAGE_SENDER_HPP_ */
