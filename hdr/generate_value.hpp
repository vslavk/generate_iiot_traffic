/*
 * generate_delay.hpp
 *
 *  Created on: 25 июн. 2019 г.
 *      Author: vslavk
 */

#ifndef HDR_GENERATE_VALUE_HPP_
#define HDR_GENERATE_VALUE_HPP_

#include "libs.hpp"
#include "../hdr/read_configuration.hpp"

//Parent class for random value generator objects
//This class realize random generator that returns constant vlf_minvalue variable
class generate_value
{
public:
	generate_value(void);
	generate_value(double _minvalue, double _probability);
	virtual ~generate_value(void);

	virtual double get_random_value(void) = 0;
	virtual void set_value_from_distr_config(distribution_config _distr_config) = 0;
	double get_probability(void);
protected:
	double vf_probability;
	double vlf_minvalue;
};



#endif /* HDR_GENERATE_VALUE_HPP_ */
