/*
 * generate_beta_delay.hpp
 *
 *  Created on: 26 июн. 2019 г.
 *      Author: vslavk
 */

#ifndef HDR_GENERATE_BETA_VALUE_HPP_
#define HDR_GENERATE_BETA_VALUE_HPP_

#include "generate_value.hpp"

//Child class for generate_value class that realize random generator by beta distribution model
class generate_beta_value : public generate_value
{
public:
	generate_beta_value(void);
	generate_beta_value(double _u, double _v);
	generate_beta_value(long double _minvalue, double _probability, double _u, double _v);
	~generate_beta_value(void);

	double get_random_value(void);
	void set_value_from_distr_config(distribution_config _distr_config);
private:
	//function that used to generate vf_k and vf_h variables
	void calculate_k_h_for_beta_generator(void);

	double first_yonka_method(void);
	double second_method(void);

	double vf_u;
	double vf_v;

	//some addition properties that used to generate random variable
	double vf_k;
	double vf_h;
};

#endif /* HDR_GENERATE_BETA_VALUE_HPP_ */
