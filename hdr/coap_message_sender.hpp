/*
 * coap_sender.hpp
 *
 *  Created on: 9 июл. 2019 г.
 *      Author: vslavk
 */

#ifndef HDR_COAP_MESSAGE_SENDER_HPP_
#define HDR_COAP_MESSAGE_SENDER_HPP_

#include "libs.hpp"
#include "message_sender.hpp"

//Child class for message_sender object which use coap transmission protocol
class coap_message_sender : public message_sender
{
public:
	coap_message_sender(void);
	coap_message_sender(std::string _message, const char* _host_addr = "0.0.0.0", uint16_t _port = 9999);
	~coap_message_sender(void);

	void send_message(void);
private:
	int resolve_address(coap_address_t *dst);
	static void onReceiveMessage(const coap_endpoint_t *local_interface, const coap_address_t *remote,
								coap_pdu_t *sent, coap_pdu_t *received, const coap_tid_t id);
};


#endif /* HDR_COAP_MESSAGE_SENDER_HPP_ */
