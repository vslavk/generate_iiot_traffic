/*
 * generate_const_value.hpp
 *
 *  Created on: 30 июл. 2019 г.
 *      Author: vslavk
 */

#ifndef HDR_GENERATE_CONST_VALUE_HPP_
#define HDR_GENERATE_CONST_VALUE_HPP_

#include "generate_value.hpp"

//Child class for generate_value class that realize random generator by uniform distribution model
class generate_const_value : public generate_value
{
public:
	generate_const_value(void);
	generate_const_value(long double _minvalue, double _probability);
	~generate_const_value(void);

	double get_random_value(void);
	void set_value_from_distr_config(distribution_config _distr_config);
};

#endif /* HDR_GENERATE_CONST_VALUE_HPP_ */
