/*
 * generate_uniform_delay.hpp
 *
 *  Created on: 26 июн. 2019 г.
 *      Author: vslavk
 */

#ifndef HDR_GENERATE_UNIFORM_VALUE_HPP_
#define HDR_GENERATE_UNIFORM_VALUE_HPP_

#include "generate_value.hpp"

//Child class for generate_value class that realize random generator by uniform distribution model
class generate_uniform_value : public generate_value
{
public:
	generate_uniform_value(void);
	generate_uniform_value(double _x, double _y);
	generate_uniform_value(long double _minvalue, double _probability, double _x, double _y);
	~generate_uniform_value(void);

	double get_random_value(void);
	void set_value_from_distr_config(distribution_config _distr_config);
private:
	double vf_x;
	double vf_y;
};

#endif /* HDR_GENERATE_UNIFORM_VALUE_HPP_ */
