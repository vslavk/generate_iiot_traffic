/*
 * generate_veibull_delay.hpp
 *
 *  Created on: 26 июн. 2019 г.
 *      Author: vslavk
 */

#ifndef HDR_GENERATE_VEIBULL_VALUE_HPP_
#define HDR_GENERATE_VEIBULL_VALUE_HPP_

#include "generate_value.hpp"

//Child class for generate_value class that realize random generator by veibull distribution model
class generate_veibull_value : public generate_value
{
public:
	generate_veibull_value(void);
	generate_veibull_value(double _lambda, double _alpha);
	generate_veibull_value(long double _minvalue, double _probability, double _lambda, double _alpha);
	~generate_veibull_value(void);

	double get_random_value(void);
	void set_value_from_distr_config(distribution_config _distr_config);
private:
	double vf_lambda;
	double vf_alpha;
};

#endif /* HDR_GENERATE_VEIBULL_VALUE_HPP_ */
