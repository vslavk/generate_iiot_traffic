#ifndef HDR_CONFIGURATION_SERVER_HPP_
#define HDR_CONFIGURATION_SERVER_HPP_

#include "libs.hpp"
#include "read_configuration.hpp"
#include "internal_functions.hpp"

class configuration_server
{
public:
	configuration_server(const char* _host, const char* _port);
	void serve_forewer(void);
private:
	int listener;
	struct sockaddr_in addr;
};

#endif /* HDR_CONFIGURATION_SERVER_HPP_ */
