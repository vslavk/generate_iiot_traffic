/*
 * generate_random_values.hpp
 *
 *  Created on: 18 июл. 2019 г.
 *      Author: vslavk
 */

#ifndef HDR_GENERATE_RANDOM_VALUES_HPP_
#define HDR_GENERATE_RANDOM_VALUES_HPP_

//Header which include all exist random value generator classes
#include "generate_value.hpp"
#include "generate_beta_value.hpp"
#include "generate_erlang_value.hpp"
#include "generate_exp_value.hpp"
#include "generate_gamma_value.hpp"
#include "generate_uniform_value.hpp"
#include "generate_veibull_value.hpp"
#include "generate_const_value.hpp"


#endif /* HDR_GENERATE_RANDOM_VALUES_HPP_ */
