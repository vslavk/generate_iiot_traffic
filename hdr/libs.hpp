/*
 * libs.h
 *
 *  Created on: 25 июн. 2019 г.
 *      Author: vslavk
 */

#ifndef HDR_LIBS_HPP_
#define HDR_LIBS_HPP_

//Some C libraries
#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <cstring>
#include <ctime>
#include <cstdbool>
#include <cmath>
#include <unistd.h>

//Some C libraries for data transmitting
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <signal.h>
#include <netdb.h>

//Libcoap libraries
#include <coap2/coap.h>

//GNU Multiple Precision Arithmetic Library
#include <gmp.h>
//GNU Multiple Precision Floating-Point Reliably Library
#include <mpfr.h>

//Some C++ libraries
#include <iostream>
#include <vector>
#include <sstream>
#include <string>
#include <memory>
#include <thread>
#include <chrono>
#include <random>
#include <memory>

//RapidJSON headers
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

//Some definitions
#define ACCURANCY 0.000001;
#define CONNMAX 1000

#endif /* HDR_LIBS_HPP_ */
