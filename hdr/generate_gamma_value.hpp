/*
 * generate_gamma_delay.hpp
 *
 *  Created on: 25 июн. 2019 г.
 *      Author: vslavk
 */

#ifndef HDR_GENERATE_GAMMA_VALUE_HPP_
#define HDR_GENERATE_GAMMA_VALUE_HPP_

#include "generate_value.hpp"

//Child class for generate_value class that realize random generator by gamma distribution model
class generate_gamma_value : public generate_value
{
public:
	generate_gamma_value(void);
	generate_gamma_value(double _lambda, double _alpha);
	generate_gamma_value(long double _minvalue, double _probability, double _lambda, double _alpha);
	~generate_gamma_value(void);

	double get_random_value(void);
	void set_value_from_distr_config(distribution_config _distr_config);
private:
	double get_rand_int_value(double lambda_int);
	double get_rand_frac_value(double lambda_int, double lambda_frac);

	double first_method(void);
	double second_method(void);

	double vf_lambda;
	double vf_alpha;
};


#endif /* HDR_GENERATE_GAMMA_VALUE_HPP_ */
