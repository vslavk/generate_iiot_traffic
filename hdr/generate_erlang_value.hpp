/*
 * generate_erlang_delay.hpp
 *
 *  Created on: 26 июн. 2019 г.
 *      Author: vslavk
 */

#ifndef HDR_GENERATE_ERLANG_VALUE_HPP_
#define HDR_GENERATE_ERLANG_VALUE_HPP_

#include "generate_value.hpp"

//Child class for generate_value class that realize random generator by erlang distribution model
class generate_erlang_value : public generate_value
{
public:
	generate_erlang_value(void);
	generate_erlang_value(int _m, double _alpha);
	generate_erlang_value(long double _minvalue, double _probability, int _m, double _alpha);
	~generate_erlang_value(void);

	double get_random_value(void);
	void set_value_from_distr_config(distribution_config _distr_config);
private:
	int vf_m;
	double vf_alpha;
};

#endif /* HDR_GENERATE_ERLANG_VALUE_HPP_ */
